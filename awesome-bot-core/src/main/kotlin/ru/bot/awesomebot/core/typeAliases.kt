package ru.bot.awesomebot.core

typealias TgMessage = org.telegram.telegrambots.meta.api.objects.Message
typealias TgChat = org.telegram.telegrambots.meta.api.objects.Chat
typealias TgUser = org.telegram.telegrambots.meta.api.objects.User
