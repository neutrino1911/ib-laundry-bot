package ru.bot.awesomebot.core.dispatching

import mu.KotlinLogging
import org.springframework.aop.support.AopUtils
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.ApplicationContext
import org.springframework.context.event.EventListener
import org.springframework.core.annotation.Order
import org.springframework.http.server.PathContainer
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.util.RouteMatcher
import org.springframework.web.util.pattern.PathPatternParser
import org.springframework.web.util.pattern.PathPatternRouteMatcher
import org.telegram.telegrambots.meta.api.methods.BotApiMethod
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import ru.bot.awesomebot.core.BusinessException
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.event.CommandEvent
import ru.bot.awesomebot.core.util.sendError
import ru.bot.awesomebot.core.util.sendMessage
import java.io.Serializable
import kotlin.reflect.KFunction
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.functions
import kotlin.reflect.full.valueParameters

private val log = KotlinLogging.logger {}

@Order
@Component
class CommandDispatcher(
        private val bot: TelegramBot,
        private val applicationContext: ApplicationContext,
        private val requestParamExtractor: RequestParamExtractor,
) : InitializingBean {

    private lateinit var handlers: Map<String, HandlerData>

    private val routeMatcher: RouteMatcher by lazy {
        PathPatternRouteMatcher(PathPatternParser().apply {
            pathOptions = PathContainer.Options.HTTP_PATH
        })
    }

    override fun afterPropertiesSet() {
        val beans = applicationContext.getBeansWithAnnotation(Component::class.java)
        val map = mutableMapOf<String, HandlerData>()
        for ((name, bean) in beans) {
            val beanClass = AopUtils.getTargetClass(bean).kotlin
            val rootPath = beanClass.findAnnotation<CommandMapping>()?.path?.firstOrNull() ?: ""
            for (function in beanClass.functions) {
                function.findAnnotation<CommandMapping>()?.let { annotation ->
                    for (childPath in annotation.path) {
                        val path = rootPath + childPath
                        if (map.containsKey(path)) {
                            error("Handler for path: $path has already registered")
                        }
                        map[path] = HandlerData(path, name, bean, function, annotation.parseMode, annotation.notification)
                    }
                }
            }
        }
        handlers = map
    }

    @Async
    @EventListener(CommandEvent::class)
    fun handleMessageEvent(event: CommandEvent) {
        log.debug { event.message.text }

        val message = event.message
        try {
            val (command, direct) = event.command
            val route = routeMatcher.parseRoute(command)
            val handlerData = findHandler(command, route)
            if (handlerData != null) {
                val params = routeMatcher.matchAndExtract(handlerData.path, route) ?: mapOf()
                val response = invoke(InvokeData(handlerData.bean, handlerData.function, message, params))
                response?.let { sendResponse(message, handlerData, response) }
            } else {
                if (direct || message.isUserMessage) bot.sendError(message)
            }
        } catch (e: TelegramApiException) {
            log.error(e.toString(), e)
            bot.sendError(message)
        } catch (e: BusinessException) {
            bot.sendMessage(message) { text = e.message!! }
        } catch (e: Exception) {
            log.error(e.message, e)
            bot.sendError(message)
        }
    }

    private fun findHandler(command: String, route: RouteMatcher.Route): HandlerData? {
        val matched = handlers.filterKeys { routeMatcher.match(it, route) }.values
        if (matched.size > 1) {
            val paths = matched.joinToString(separator = "; ") { it.path }
            error("There are several handlers matched [$command] command. $paths")
        }
        return matched.firstOrNull()
    }

    private fun invoke(invokeData: InvokeData): Any? {
        val (bean, function, message, params) = invokeData
        val args = arrayOfNulls<Any>(function.valueParameters.size)

        function.valueParameters.forEachIndexed { index, param ->
            args[index] = requestParamExtractor.extractParam(message, param, params)
        }

        return function.call(bean, *args)
    }

    private fun sendResponse(message: Message, handlerData: HandlerData, response: Any) {
        when (response) {
            is String -> bot.sendMessage(message) {
                if (response.isEmpty()) return@sendMessage
                text = response
                when (handlerData.parseMode) {
                    ParseMode.MARKDOWN -> enableMarkdown(true)
                    ParseMode.MARKDOWNV2 -> enableMarkdownV2(true)
                    ParseMode.HTML -> enableHtml(true)
                }
                if (handlerData.notification) enableNotification() else disableNotification()
            }
            is BotApiMethod<*> -> bot.execute(response as BotApiMethod<Serializable>)
        }
    }

    private data class HandlerData(
            val path: String,
            val beanName: String,
            val bean: Any,
            val function: KFunction<*>,
            val parseMode: ParseMode,
            val notification: Boolean
    )

    private data class InvokeData(
            val bean: Any,
            val function: KFunction<*>,
            val message: Message,
            val params: Map<String, String>
    )

}
