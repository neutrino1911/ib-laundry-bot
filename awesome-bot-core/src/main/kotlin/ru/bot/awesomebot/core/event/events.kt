package ru.bot.awesomebot.core.event

import org.springframework.context.ApplicationEvent
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update

interface HasMessage {
    val message: Message
}

interface HasUpdate {
    val update: Update
}

class CommandEvent(
        source: Any,
        override val message: Message,
        val command: Command
) : HasMessage, ApplicationEvent(source)

data class Command(val command: String, val direct: Boolean)

class MemberLeftChatEvent(source: Any, override val update: Update) : HasUpdate, ApplicationEvent(source)

class MessageEvent(source: Any, override val message: Message) : HasMessage, ApplicationEvent(source)

class NewChatMembersEvent(source: Any, override val update: Update) : HasUpdate, ApplicationEvent(source)

class UpdateEvent(source: Any, override val update: Update) : HasUpdate, ApplicationEvent(source)
