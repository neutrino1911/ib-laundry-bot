package ru.bot.awesomebot.core.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.telegram.telegrambots.bots.DefaultBotOptions.ProxyType

@ConstructorBinding
@ConfigurationProperties("telegram.bot")
data class TelegramBotProperties(
        val token: String,
        val identifier: String,
        val superadmin: Set<Int> = setOf(),
        val proxy: Proxy = Proxy()
)

data class Proxy(
        val type: ProxyType = ProxyType.NO_PROXY,
        val host: String? = null,
        val port: Int = 0
)
