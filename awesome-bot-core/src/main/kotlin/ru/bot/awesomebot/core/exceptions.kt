package ru.bot.awesomebot.core

open class BusinessException(message: String) : RuntimeException(message)

class InsufficientPermissionException(message: String) : BusinessException(message)
