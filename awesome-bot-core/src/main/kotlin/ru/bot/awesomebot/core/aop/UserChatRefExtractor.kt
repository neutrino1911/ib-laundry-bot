package ru.bot.awesomebot.core.aop

import org.aspectj.lang.ProceedingJoinPoint
import org.springframework.stereotype.Component
import ru.bot.awesomebot.core.extraction.VirtualChatIdExtractorChain
import ru.bot.awesomebot.core.extraction.VirtualUserIdExtractorChain

@Component
class UserChatRefExtractor(
        private val userIdExtractor: VirtualUserIdExtractorChain<*>,
        private val chatIdExtractor: VirtualChatIdExtractorChain<*>
) {

    fun extract(pjp: ProceedingJoinPoint): UserChatRef? {
        val userId = userIdExtractor.extractUserId(pjp)
        val chatId = chatIdExtractor.extractChatId(pjp)
        return if (userId != null && chatId != null) {
            UserChatRef(chatId, userId)
        } else {
            null
        }
    }

}

data class UserChatRef(val chatId: Long, val userId: Long)
