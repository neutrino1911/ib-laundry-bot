package ru.bot.awesomebot.core

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.DefaultBotOptions
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession
import ru.bot.awesomebot.core.config.TelegramBotProperties
import ru.bot.awesomebot.core.event.MessagePublisher
import ru.bot.awesomebot.core.event.UpdatePublisher

@Component
class TelegramBot(
        private val props: TelegramBotProperties,
        private val messagePublishers: List<MessagePublisher>,
        private val updatePublishers: List<UpdatePublisher>
) : TelegramLongPollingBot(init(props)), ApplicationRunner {

    override fun onUpdateReceived(update: Update) {
        updatePublishers.forEach { it.publish(update) }
        if (update.hasMessage()) {
            val message = update.message
            messagePublishers.forEach { it.publish(message) }
        }
    }

    override fun getBotUsername() = "bot"

    override fun getBotToken() = props.token

    override fun run(args: ApplicationArguments) {
        TelegramBotsApi(DefaultBotSession::class.java).registerBot(this)
    }

}

private fun init(props: TelegramBotProperties) = DefaultBotOptions().apply {
    proxyType = props.proxy.type
    proxyHost = props.proxy.host
    proxyPort = props.proxy.port
}
