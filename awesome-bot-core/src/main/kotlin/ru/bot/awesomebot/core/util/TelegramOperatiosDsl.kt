package ru.bot.awesomebot.core.util

import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.TelegramBot

fun TelegramBot.deleteMessage(message: Message): Boolean {
    return execute(DeleteMessage().apply {
        chatId = message.chatId.toString()
        messageId = message.messageId
    })
}

fun TelegramBot.deleteMessage(message: Message, init: DeleteMessage.() -> Unit): Boolean {
    return execute(DeleteMessage().apply {
        chatId = message.chatId.toString()
        messageId = message.messageId
        apply(init)
    })
}

fun TelegramBot.sendMessage(init: SendMessage.() -> Unit): Message {
    return execute(SendMessage().apply(init))
}

fun TelegramBot.sendMessage(message: Message, init: SendMessage.() -> Unit): Message {
    return sendMessage {
        chatId = message.chatId!!.toString()
        replyToMessageId = message.messageId
        apply(init)
    }
}

fun TelegramBot.sendError(chatId: Long, userId: Long): Message {
    return sendMessage {
        this.chatId = chatId.toString()
        text = "[Ошибка]"
    }
}

fun TelegramBot.sendError(message: Message): Message {
    return sendMessage(message) {
        text = "[Ошибка]"
    }
}

fun deleteMessage(message: Message, init: DeleteMessage.() -> Unit): DeleteMessage {
    return DeleteMessage().apply {
        chatId = message.chatId.toString()
        messageId = message.messageId
        apply(init)
    }
}
