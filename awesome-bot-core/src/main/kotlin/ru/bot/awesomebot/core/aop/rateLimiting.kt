package ru.bot.awesomebot.core.aop

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import ru.bot.awesomebot.core.extraction.VirtualChatIdExtractorChain
import java.util.concurrent.ConcurrentHashMap

@Target(AnnotationTarget.FUNCTION)
annotation class OnlyOneAtTheMoment

@Aspect
@Component
@Order(-20)
class RateAdvice(private val chatIdExtractor: VirtualChatIdExtractorChain<*>) {

    private val inProgress: MutableSet<Key> = ConcurrentHashMap.newKeySet()

    @Around("@annotation(annotation)")
    fun around(pjp: ProceedingJoinPoint, annotation: OnlyOneAtTheMoment): Any? {
        val chatId = chatIdExtractor.extractChatId(pjp) ?: return pjp.proceed()
        val key = Key(pjp.signature.toLongString(), chatId)
        return if (inProgress.add(key)) {
            try {
                pjp.proceed()
            } finally {
                inProgress.remove(key)
            }
        } else null
    }

    private data class Key(val signature: String, val chatId: Long)
}
