package ru.bot.awesomebot.core.dispatching

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class CommandMapping(
        vararg val path: String,
        val parseMode: ParseMode = ParseMode.NONE,
        val notification: Boolean = true
)

@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class CommandParam(val name: String = "")

class CmdArgs(collection: Collection<String>) : ArrayList<String>(collection)

enum class ParseMode {
    NONE, MARKDOWN, MARKDOWNV2, HTML
}
