ALTER TABLE advice_history
    ADD CONSTRAINT UQ_advice_history UNIQUE (user_id, date)
