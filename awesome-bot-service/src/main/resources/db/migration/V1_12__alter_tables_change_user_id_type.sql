ALTER TABLE "message"
    ALTER COLUMN user_id TYPE INT;
ALTER TABLE "user"
    ALTER COLUMN id TYPE INT;
ALTER TABLE user_to_chat
    ALTER COLUMN user_id TYPE INT;
ALTER TABLE advice_got
    ALTER COLUMN user_id TYPE INT;
ALTER TABLE potd_result
    ALTER COLUMN user_id TYPE INT;
