CREATE TABLE IF NOT EXISTS function
(
    chat_id BIGINT  NOT NULL,
    name    VARCHAR NOT NULL,
    "value" VARCHAR NOT NULL,

    CONSTRAINT function_pk PRIMARY KEY (chat_id, name),
    CONSTRAINT function_chat_fk FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE
);
