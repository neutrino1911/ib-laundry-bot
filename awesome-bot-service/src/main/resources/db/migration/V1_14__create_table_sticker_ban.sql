CREATE TABLE IF NOT EXISTS sticker_ban
(
    chat_id BIGINT  NOT NULL,
    key     VARCHAR NOT NULL,
    is_pack BOOLEAN NOT NULL DEFAULT FALSE,

    CONSTRAINT sticker_ban_pk PRIMARY KEY (chat_id, key),
    CONSTRAINT sticker_ban_fk FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE
);
