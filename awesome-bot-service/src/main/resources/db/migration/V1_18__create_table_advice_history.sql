CREATE TABLE IF NOT EXISTS advice_history
(
    id        BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
    advice_id INT                                 NOT NULL,
    user_id   INT                                 NOT NULL,
    "date"    DATE                                NOT NULL DEFAULT current_date,
    text      VARCHAR                             NOT NULL,
    CONSTRAINT advice_history_pk PRIMARY KEY (id),
    CONSTRAINT advice_history_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE
);
