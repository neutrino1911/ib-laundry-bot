CREATE TABLE IF NOT EXISTS "user"
(
  id         BIGINT       NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name  VARCHAR(100),
  user_name  VARCHAR(100),
  is_bot     BOOLEAN      NOT NULL,
  CONSTRAINT user_pk PRIMARY KEY (id)
);
