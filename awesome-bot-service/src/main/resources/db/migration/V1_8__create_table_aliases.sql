CREATE TABLE IF NOT EXISTS alias
(
    chat_id BIGINT  NOT NULL,
    key     VARCHAR NOT NULL,
    "value" VARCHAR NOT NULL,

    CONSTRAINT aliases_unique_key PRIMARY KEY (chat_id, key),
    CONSTRAINT aliases_chat_fk FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE
);
