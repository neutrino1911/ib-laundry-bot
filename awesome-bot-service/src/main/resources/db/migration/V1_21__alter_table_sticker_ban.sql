UPDATE sticker_ban
SET "key" = "hash"
WHERE is_pack = 'f';

ALTER TABLE sticker_ban
    DROP COLUMN is_pack,
    DROP COLUMN hash;
