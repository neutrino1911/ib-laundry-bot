CREATE TABLE IF NOT EXISTS potd_registry
(
  chat_id   BIGINT  NOT NULL,
  user_id   BIGINT  NOT NULL,
  is_active BOOLEAN NOT NULL,
  CONSTRAINT potd_registry_pk PRIMARY KEY (chat_id, user_id),
  CONSTRAINT potd_registry_chat_fk FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE,
  CONSTRAINT potd_registry_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE
);
