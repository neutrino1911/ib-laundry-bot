CREATE TABLE IF NOT EXISTS message
(
  id         BIGINT                      NOT NULL,
  chat_id    BIGINT                      NOT NULL,
  user_id    BIGINT                      NOT NULL,
  "date"     TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  text       VARCHAR(4096),
  is_sticker BOOLEAN                     NOT NULL,
  CONSTRAINT message_pk PRIMARY KEY (id),
  CONSTRAINT message_chat_fk FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE,
  CONSTRAINT message_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE
);
