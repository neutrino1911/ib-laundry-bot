CREATE TABLE IF NOT EXISTS advice_got
(
  id      BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
  user_id BIGINT                              NOT NULL,
  "date"  DATE                                NOT NULL,
  CONSTRAINT advice_got_pk PRIMARY KEY (id),
  CONSTRAINT advice_got_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE
);
