package ru.bot.awesomebot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AwesomeBotApplication

fun main(args: Array<String>) {
    runApplication<AwesomeBotApplication>(*args)
}
