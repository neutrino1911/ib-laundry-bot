package ru.bot.awesomebot.service.feign

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping

@FeignClient(name = "funpics", url = "https://stardisk.xyz/projects")
interface FunpicsClient {
    @PostMapping(path = ["/memasiki/memasiki.php"], consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun memasiki(form: Map<String, *>): String

    @PostMapping(path = ["/funpics/funpics.php"], consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    fun funpics(form: Map<String, *>): String

    @GetMapping(path = ["/funpics/{filename}"])
    fun download(@PathVariable("filename") filename: String): ByteArray
}
