package ru.bot.awesomebot.service

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component
import java.sql.ResultSet
import java.time.LocalDate

@Component
class PotdStatisticService(
        private val jdbcTemplate: JdbcTemplate
) {

    fun fetchTop(chatId: Long, count: Int, all: Boolean): String {
        //language=PostgreSQL
        val query = """
            SELECT u.id,
                   u.first_name,
                   u.last_name,
                   u.user_name,
                   count(pr.id) cnt
            FROM potd_result pr
            JOIN "user" u ON u.id = pr.user_id
            WHERE pr.chat_id = ? AND pr.date >= ?
            GROUP BY u.id, u.first_name, u.last_name, u.user_name
            ORDER BY cnt DESC
            LIMIT ?
        """.trimIndent()

        val dateBound = if (all) LocalDate.ofYearDay(1970, 1) else LocalDate.now().withDayOfYear(1)

        val list = jdbcTemplate.query(query, ::rowMapper, chatId, dateBound, count)

        return buildString {
            append("Топ-$count **пидоров**\n")
            for (index in list.indices) {
                val entry = list[index]
                append('\n')
                append(index + 1)
                append(". ")
                append(entry.getUserMention())
                append(" – ")
                append(entry.count)
                append(' ')
                append(getCountWord(entry.count))
            }
        }
    }

    private fun PotdStatsEntry.getUserMention(): String {
        val name = ("$firstName $lastName").replace("null", "").trim()
        return "[$name](tg://user?id=$userId)"
    }

    private fun getCountWord(count: Int) = when {
        count in 11..19 -> "раз"
        count % 10 == 1 -> "раз"
        count % 10 in 2..4 -> "раза"
        else -> "раз"
    }

    private fun rowMapper(rs: ResultSet, rowNum: Int) = PotdStatsEntry(
            userId = rs.getLong("id"),
            firstName = rs.getString("first_name"),
            lastName = rs.getString("last_name"),
            userName = rs.getString("user_name"),
            count = rs.getInt("cnt")
    )

    private data class PotdStatsEntry(
            val firstName: String,
            val lastName: String? = null,
            val userName: String? = null,
            val userId: Long,
            val count: Int = 0
    )

}
