package ru.bot.awesomebot.service.feign

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import ru.bot.awesomebot.data.Advice

@FeignClient(name = "advice", url = "http://fucking-great-advice.ru/api")
interface AdviceClient {
    @GetMapping("/random")
    fun random(): Advice
}
