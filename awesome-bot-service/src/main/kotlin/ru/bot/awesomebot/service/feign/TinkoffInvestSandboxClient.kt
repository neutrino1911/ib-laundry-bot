package ru.bot.awesomebot.service.feign

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader

@FeignClient(name = "tinkoffSandbox", url = "https://api-invest.tinkoff.ru/openapi/sandbox")
interface TinkoffInvestSandboxClient {
    @GetMapping(path = ["/market/orderbook?figi={figi}&depth={depth}"])
    fun orderbook(
        @PathVariable figi: String,
        @PathVariable depth: Int,
        @RequestHeader("Authorization") auth: String
    ): OrderbookResponse

    @GetMapping(path = ["/market/search/by-ticker?ticker={ticker}"])
    fun byTicker(
        @PathVariable ticker: String,
        @RequestHeader("Authorization") auth: String
    ): TickerResponse

}

data class OrderbookResponse(
    val trackingId: String,
    val status: String,
    val payload: Orderbook
)

data class Orderbook(
    val figi: String,
    val depth: Int,
    val bids: List<OrderResponse>,
    val asks: List<OrderResponse>,
    val tradeStatus: String,
    val minPriceIncrement: Double,
    val faceValue: Double,
    val lastPrice: Double,
    val closePrice: Double,
    val limitUp: Double,
    val limitDown: Double
)

data class OrderResponse(
    val price: Double,
    val quantity: Int
)

data class TickerResponse(
    val trackingId: String,
    val status: String,
    val payload: TickerResponsePayload
)

data class TickerResponsePayload(
    val total: String,
    val instruments: List<TickerResponseInstruments>
)

data class TickerResponseInstruments(
    val figi: String,
    val ticker: String
)
