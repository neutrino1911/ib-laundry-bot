package ru.bot.awesomebot.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import ru.bot.awesomebot.data.entity.PotdResult
import ru.bot.awesomebot.data.entity.User
import ru.bot.awesomebot.data.entity.UserToChatId
import ru.bot.awesomebot.data.repository.PotdResultRepository
import ru.bot.awesomebot.data.repository.UserRepository
import ru.bot.awesomebot.data.repository.UserToChatRepository
import java.time.LocalDate

@Service
class PotdService(
        private val userToChatService: UserToChatService,
        private val userToChatRepository: UserToChatRepository,
        private val potdResultRepository: PotdResultRepository,
        private val userRepository: UserRepository
) {
    @Transactional
    fun register(chatId: Long, userId: Long): Boolean {
        val link = userToChatRepository.findByIdOrNull(UserToChatId(chatId, userId))
                ?: userToChatService.addLink(chatId, userId)
        if (!link.potdParticipant) {
            userToChatRepository.save(link.copy(potdParticipant = true))
            return true
        }
        return false
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun play(chatId: Long): User? {
        val players = userToChatRepository.findAllByChatIdAndActiveTrueAndPotdParticipantTrue(chatId)
        repeat(players.size) { players.shuffled() }
        val (_, userId) = players.random()
        potdResultRepository.save(PotdResult(userId = userId, chatId = chatId, date = LocalDate.now()))
        return userRepository.findByIdOrNull(userId)
    }

    fun getWinner(chatId: Long): User? {
        return potdResultRepository.findByChatIdAndDate(chatId, LocalDate.now())?.let {
            userRepository.findByIdOrNull(it.userId)
        }
    }
}
