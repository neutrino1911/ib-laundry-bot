package ru.bot.awesomebot.service

import mu.KotlinLogging
import org.apache.commons.codec.binary.Base64
import org.springframework.stereotype.Service
import ru.bot.awesomebot.service.feign.FunpicsClient

private val log = KotlinLogging.logger {}

@Service
class FunpicsService(
        private val funpicsClient: FunpicsClient
) {
    fun generateMemasiki(templateNumber: String, text: String): ByteArray {
        val map = mapOf(
                "template" to "template$templateNumber.jpg",
                "template_custom" to "",
                "word" to text
        )
        val base64 = funpicsClient.memasiki(map)
        return getBytesFromBase64(base64)
    }

    fun generateFunpics(text: String): ByteArray {
        val map = mapOf("word" to text)
        val filename = funpicsClient.funpics(map)
        return funpicsClient.download(filename)
    }

    private fun getBytesFromBase64(base64: String): ByteArray {
        if (base64.startsWith("data:image/jpeg;base64")) {
            return Base64.decodeBase64(base64.substringAfter(','))
        } else {
            log.error(base64)
            throw RuntimeException()
        }
    }

}
