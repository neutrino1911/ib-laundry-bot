package ru.bot.awesomebot.service

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.telegram.telegrambots.meta.api.objects.Chat
import ru.bot.awesomebot.core.BusinessException
import ru.bot.awesomebot.data.entity.Stock
import ru.bot.awesomebot.data.entity.StockId
import ru.bot.awesomebot.data.repository.StockRepository
import ru.bot.awesomebot.service.feign.TinkoffInvestSandboxClient
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

@Service
class StockService(
    private val stockRepository: StockRepository,
    @Value("\${tinkoff.api.token}")
    private val tinkoffToken: String,
    private val tinkoffInvestSandboxClient: TinkoffInvestSandboxClient,
) {
    private val log = KotlinLogging.logger {}
    private val auth = "Bearer $tinkoffToken"
    private val formatter = DecimalFormat("+0.##;-0.##").apply {
        decimalFormatSymbols = DecimalFormatSymbols().apply {
            decimalSeparator = '.'
        }
    }

    @Transactional
    fun set(chat: Chat, key: String, ticker: String) {
        val tickerResponse = tinkoffInvestSandboxClient.byTicker(ticker, auth)
        val figi = tickerResponse.payload.instruments.firstOrNull()?.figi
            ?: throw BusinessException("Ticker '$ticker' not found")

        stockRepository.save(Stock(chatId = chat.id, key = key, ticker = ticker, figi = figi))
    }

    fun get(chat: Chat, key: String): String? {
        var stock = stockRepository.findByIdOrNull(StockId(chat.id, key)) ?: return null

        //Backward compatibility
        if (stock.figi == null) {
            set(chat, key, stock.ticker)
            stock = stockRepository.findByIdOrNull(StockId(chat.id, key)) ?: return null
        }

        return stock.figi?.let { "$key по ${formatTicker(it)}" }
    }

    fun getAliases(chat: Chat): List<String> {
        return stockRepository.findAllByChatId(chat.id).map { it.key }
    }

    @Transactional
    fun del(chat: Chat, key: String) {
        stockRepository.deleteById(StockId(chat.id, key))
    }

    private fun formatTicker(figi: String): String {
        val orderbook = tinkoffInvestSandboxClient.orderbook(figi, 1, auth)
        log.info { orderbook.toString() }

        val lastPrice = orderbook.payload.lastPrice
        val closePrice = orderbook.payload.closePrice
        val dayChange = lastPrice - closePrice
        val percentChange = (dayChange / closePrice).toBigDecimal()
            .movePointRight(2)
            .setScale(2, RoundingMode.HALF_UP)

        val changeString = formatter.format(percentChange)

        return "$lastPrice ($changeString%)"
    }
}
