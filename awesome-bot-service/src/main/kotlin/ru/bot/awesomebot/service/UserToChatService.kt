package ru.bot.awesomebot.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.bot.awesomebot.data.entity.UserToChat
import ru.bot.awesomebot.data.entity.UserToChatId
import ru.bot.awesomebot.data.repository.UserToChatRepository

@Service
class UserToChatService(
        private val userToChatRepository: UserToChatRepository
) {
    @Transactional
    fun activate(chatId: Long, userId: Long) {
        changeStatus(chatId, userId, true)
    }

    @Transactional
    fun deactivate(chatId: Long, userId: Long) {
        changeStatus(chatId, userId, false)
    }

    @Transactional
    fun addLink(chatId: Long, userId: Long): UserToChat {
        return doIfExists(chatId, userId) {
            if (!it.active) userToChatRepository.save(it.copy(active = true)) else it
        }
    }

    private fun changeStatus(chatId: Long, userId: Long, status: Boolean) {
        doIfExists(chatId, userId) { link ->
            if (link.active != status) {
                userToChatRepository.save(link.copy(active = status, potdParticipant = true))
            } else {
                link
            }
        }
    }

    private fun doIfExists(chatId: Long, userId: Long, function: (UserToChat) -> UserToChat): UserToChat {
        val identity = UserToChatId(chatId, userId)
        return userToChatRepository.findByIdOrNull(identity)
                ?.let { function(it) }
                ?: run { userToChatRepository.save(UserToChat(chatId = chatId, userId = userId, id = identity)) }
    }
}
