package ru.bot.awesomebot.service

import org.apache.commons.lang3.StringUtils
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.bot.awesomebot.BanException
import ru.bot.awesomebot.data.entity.User
import ru.bot.awesomebot.data.entity.UserToChatId
import ru.bot.awesomebot.data.repository.UserRepository
import ru.bot.awesomebot.data.repository.UserToChatRepository

@Service
class BanService(
        private val userRepository: UserRepository,
        private val userToChatRepository: UserToChatRepository
) {
    @Transactional
    fun ban(chatId: Long, term: String): User {
        return ban(chatId, term, true)
    }

    @Transactional
    fun ban(chatId: Long, userId: Long): User {
        return ban(chatId, userId, true)
    }

    @Transactional
    fun unban(chatId: Long, term: String): User {
        return ban(chatId, term, false)
    }

    @Transactional
    fun unban(chatId: Long, userId: Long): User {
        return ban(chatId, userId, false)
    }

    fun isBanned(chatId: Long, userId: Long): Boolean {
        return userToChatRepository.existsByChatIdAndUserIdAndBannedTrue(chatId, userId)
    }

    fun banList(chatId: Long): List<User> {
        return userRepository.findAllByChatIdAndBannedTrue(chatId)
    }

    private fun ban(chatId: Long, term: String, banned: Boolean): User {
        val userId = when {
            term.startsWith("@") -> {
                userRepository.findByChatIdAndUsername(chatId, term.substring(1))?.id
                        ?: throw BanException("Пидор с таким userName в этом чате не найден!")
            }
            StringUtils.isNumeric(term) -> {
                term.toLong().also { userId ->
                    if (!userToChatRepository.existsByChatIdAndUserId(chatId, userId)) {
                        throw BanException("Пидор с таким id не найден!")
                    }
                }
            }
            else -> {
                val list = userRepository.findAllByFirstNameAndChatId(term, chatId)
                when {
                    list.isEmpty() -> throw BanException("Пидор с таким firstName в этом чате не найден!")
                    list.size > 1 -> throw BanException("В этом чате несколько пидоров с таким firstName!")
                    else -> list[0].id
                }
            }
        }
        return ban(chatId, userId, banned)
    }

    private fun ban(chatId: Long, userId: Long, banned: Boolean): User {
        val link = userToChatRepository.findByIdOrNull(UserToChatId(chatId, userId))
                ?.copy(banned = banned)
                ?: throw BanException("Пидор с таким id в этом чате не найден!")
        userToChatRepository.save(link)
        return userRepository.findByIdOrNull(userId)
                ?: throw BanException("Пидор с таким id не найден!")
    }

}
