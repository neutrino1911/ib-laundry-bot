package ru.bot.awesomebot.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.telegram.telegrambots.meta.api.objects.Chat
import ru.bot.awesomebot.data.entity.Alias
import ru.bot.awesomebot.data.entity.AliasId
import ru.bot.awesomebot.data.entity.User
import ru.bot.awesomebot.data.repository.AliasRepository
import ru.bot.awesomebot.data.repository.UserRepository

@Service
class AliasService(
        private val aliasRepository: AliasRepository,
        private val userRepository: UserRepository
) {
    @Transactional
    fun set(chat: Chat, key: String, value: String) {
        val alias = doIfMention(key) { username ->
            userRepository.findByUsername(username)?.let { user ->
                Alias(chatId = chat.id, key = user.id.toString(), value = value, user = true)
            }
        } ?: Alias(chatId = chat.id, key = key, value = value)

        aliasRepository.save(alias)
    }

    fun get(chat: Chat, key: String): String? {
        return aliasRepository.findByIdOrNull(getAliasId(chat, key))?.value
    }

    fun getAliases(chat: Chat): List<String> {
        return aliasRepository.findAllByChatId(chat.id).map { obtainKey(it) }
    }

    @Transactional
    fun del(chat: Chat, key: String) {
        aliasRepository.deleteById(getAliasId(chat, key))
    }

    private fun getAliasId(chat: Chat, key: String): AliasId {
        return doIfMention(key) { username ->
            userRepository.findByUsername(username)?.let { user ->
                AliasId(chat.id, user.id.toString())
            }
        } ?: AliasId(chat.id, key)
    }

    private fun obtainKey(alias: Alias): String {
        return (if (alias.user) {
            userRepository.findByIdOrNull(alias.key.toLong())?.let { getUserMention(it) }
        } else null) ?: alias.key
    }

    private fun getUserMention(user: User) =
            if (user.username.isNullOrEmpty()) {
                "<a href=\"tg://user?id=${user.id}\">${user.firstName}</a>"
            } else {
                "<a href=\"tg://user?id=${user.id}\">@${user.username}</a>"
            }
}

private fun <T> doIfMention(key: String, function: (String) -> T) =
        if (key.startsWith("@")) function(key.substring(1)) else null
