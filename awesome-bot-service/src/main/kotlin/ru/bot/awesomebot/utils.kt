package ru.bot.awesomebot

import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import ru.bot.awesomebot.data.entity.User

fun User.getMention(): String {
    val name = ("$firstName $lastName").replace("null", "").trim()
    return "[$name](tg://user?id=$id)"
}

fun sendMessage(user: User, init: SendMessage.() -> Unit): SendMessage {
    val obj = SendMessage()
    obj.chatId = user.id.toString()
    return obj.apply(init)
}
