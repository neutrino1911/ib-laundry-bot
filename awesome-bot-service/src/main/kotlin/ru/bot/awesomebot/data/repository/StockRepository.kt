package ru.bot.awesomebot.data.repository

import org.springframework.data.repository.CrudRepository
import ru.bot.awesomebot.data.entity.Stock
import ru.bot.awesomebot.data.entity.StockId

interface StockRepository : CrudRepository<Stock, StockId> {
    fun findAllByChatId(chatId: Long): List<Stock>
}
