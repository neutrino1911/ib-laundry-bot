package ru.bot.awesomebot.data.entity

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "user_to_chat")
data class UserToChat(
        @Column(name = "chat_id", insertable = false, updatable = false)
        val chatId: Long,

        @Column(name = "user_id", insertable = false, updatable = false)
        val userId: Long,

        @EmbeddedId
        val id: UserToChatId = UserToChatId(chatId, userId),

        @Column(name = "is_active")
        val active: Boolean = true,

        @Column(name = "is_potd_participant")
        val potdParticipant: Boolean = true,

        @Column(name = "is_banned")
        val banned: Boolean = false
)

@Embeddable
data class UserToChatId(@Column(name = "chat_id") val chatId: Long, @Column(name = "user_id") val userId: Long) : Serializable
