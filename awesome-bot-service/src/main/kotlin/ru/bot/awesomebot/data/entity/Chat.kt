package ru.bot.awesomebot.data.entity

import javax.persistence.*

@Entity
@Table(name = "chat")
data class Chat(
        @Id
        val id: Long,

        @Enumerated(EnumType.STRING)
        @Column(name = "type")
        val type: ChatType,

        @Column(name = "title")
        val title: String
)

enum class ChatType {
    PRIVATE, GROUP, SUPERGROUP, CHANNEL
}
