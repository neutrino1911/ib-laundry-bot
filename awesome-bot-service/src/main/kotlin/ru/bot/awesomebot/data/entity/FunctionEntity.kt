package ru.bot.awesomebot.data.entity

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "function")
data class FunctionEntity(
        @Column(name = "chat_id", insertable = false, updatable = false)
        val chatId: Long,

        @Column(name = "name", insertable = false, updatable = false)
        val name: String,

        @EmbeddedId
        val id: FunctionId = FunctionId(chatId, name),

        @Column(name = "value")
        val value: String
)

@Embeddable
data class FunctionId(@Column(name = "chat_id") val chatId: Long, @Column(name = "name") val name: String) : Serializable
