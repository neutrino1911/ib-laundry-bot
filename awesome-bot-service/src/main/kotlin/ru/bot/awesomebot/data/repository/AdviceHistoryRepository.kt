package ru.bot.awesomebot.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.bot.awesomebot.data.entity.AdviceHistory
import java.time.LocalDate

interface AdviceHistoryRepository : JpaRepository<AdviceHistory, Long> {
    fun findByUserIdAndDate(userId: Long, date: LocalDate): AdviceHistory?
}