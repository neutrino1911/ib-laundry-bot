package ru.bot.awesomebot.data.repository

import org.springframework.data.repository.CrudRepository
import ru.bot.awesomebot.data.entity.Chat

interface ChatRepository : CrudRepository<Chat, Long>
