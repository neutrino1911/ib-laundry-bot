package ru.bot.awesomebot.data.entity

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "alias")
data class Alias(
        @Column(name = "chat_id", insertable = false, updatable = false)
        val chatId: Long,

        @Column(name = "key", insertable = false, updatable = false)
        val key: String,

        @EmbeddedId
        val id: AliasId = AliasId(chatId, key),

        @Column(name = "value")
        val value: String,

        @Column(name = "is_user")
        val user: Boolean = false
)

@Embeddable
data class AliasId(@Column(name = "chat_id") val chatId: Long, @Column(name = "key") val key: String) : Serializable
