package ru.bot.awesomebot.data.entity

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "potd_result")
data class PotdResult(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @Column(name = "user_id")
        val userId: Long,

        @Column(name = "chat_id")
        val chatId: Long,

        @Column(name = "date")
        val date: LocalDate
)
