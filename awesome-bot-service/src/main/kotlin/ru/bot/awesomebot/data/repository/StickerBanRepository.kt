package ru.bot.awesomebot.data.repository

import org.springframework.data.repository.CrudRepository
import ru.bot.awesomebot.data.entity.StickerBan
import ru.bot.awesomebot.data.entity.StickerBanId

interface StickerBanRepository : CrudRepository<StickerBan, StickerBanId> {
    fun deleteByChatIdAndKey(chatId: Long, key: String)
    fun existsByChatIdAndKey(chatId: Long, key: String): Boolean
}
