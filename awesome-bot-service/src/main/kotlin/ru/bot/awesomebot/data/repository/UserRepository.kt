package ru.bot.awesomebot.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.bot.awesomebot.data.entity.User

interface UserRepository : JpaRepository<User, Long> {

    fun findByUsername(username: String): User?

    @Query(nativeQuery = true, value = """
        SELECT * FROM "user" u
        JOIN user_to_chat uc ON uc.user_id = u.id
        WHERE uc.is_active = true AND uc.chat_id = :id
    """)
    fun findAllByChatId(id: Long): List<User>

    @Query(nativeQuery = true, value = """
        SELECT * FROM "user" u
        JOIN user_to_chat uc ON uc.user_id = u.id
        WHERE u.first_name = :firstName AND uc.chat_id = :chatId AND uc.is_active = true
    """)
    fun findAllByFirstNameAndChatId(firstName: String, chatId: Long): List<User>

    @Query(nativeQuery = true, value = """
        SELECT * FROM "user" u
        JOIN user_to_chat uc ON uc.user_id = u.id
        WHERE u.user_name = :username AND uc.chat_id = :chatId AND uc.is_active = true
    """)
    fun findByChatIdAndUsername(chatId: Long, username: String): User?

    @Query(nativeQuery = true, value = """
        SELECT * FROM "user" u
        JOIN user_to_chat uc ON uc.user_id = u.id
        WHERE u.is_banned = true AND uc.chat_id = :chatId
    """)
    fun findAllByChatIdAndBannedTrue(chatId: Long): List<User>
}
