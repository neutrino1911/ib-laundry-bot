package ru.bot.awesomebot.data.entity

import java.time.LocalDate
import javax.persistence.Table

@Table(name = "message")
data class Message(
        val id: Int,
        val chat: Chat,
        val from: User,
        val date: LocalDate,
        val text: String? = null,
        val caption: String? = null,
        val sticker: Boolean = false,
        val photo: List<Photo> = listOf()
) {
    val chatId = chat.id
}
