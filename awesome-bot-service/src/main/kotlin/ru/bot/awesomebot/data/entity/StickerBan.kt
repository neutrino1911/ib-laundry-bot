package ru.bot.awesomebot.data.entity

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "sticker_ban")
data class StickerBan(
        @Column(name = "chat_id", insertable = false, updatable = false)
        val chatId: Long,

        @Column(name = "key", insertable = false, updatable = false)
        val key: String,

        @EmbeddedId
        val id: StickerBanId = StickerBanId(chatId, key)
)

@Embeddable
data class StickerBanId(
        @Column(name = "chat_id") val chatId: Long,
        @Column(name = "key") val key: String
) : Serializable
