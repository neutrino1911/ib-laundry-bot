package ru.bot.awesomebot.data

import org.telegram.telegrambots.meta.api.objects.PhotoSize
import ru.bot.awesomebot.core.TgChat
import ru.bot.awesomebot.core.TgMessage
import ru.bot.awesomebot.core.TgUser
import ru.bot.awesomebot.data.entity.*
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset

fun TgMessage.toMessage(): Message = Message(
        id = messageId,
        chat = chat.toChat(),
        from = from.toUser(),
        date = LocalDate.ofInstant(Instant.ofEpochSecond(date.toLong()), ZoneOffset.UTC),
        text = text,
        caption = caption,
        sticker = hasSticker(),
        photo = photo?.map { it.toPhoto() } ?: listOf()
)

fun TgChat.toChat(): Chat = Chat(
        id = id,
        type = when {
            isUserChat -> ChatType.PRIVATE
            isGroupChat -> ChatType.GROUP
            isSuperGroupChat -> ChatType.SUPERGROUP
            isChannelChat -> ChatType.CHANNEL
            else -> error("Unknown chat type: $this")
        },
        title = title ?: "$firstName $lastName".replace("null", "").trim()
)

fun TgUser.toUser(): User = User(
        id = id,
        firstName = firstName,
        lastName = lastName,
        username = userName,
        bot = isBot
)

fun PhotoSize.toPhoto(): Photo = Photo(
        fileId = fileId,
        width = width,
        height = height,
        fileSize = fileSize,
        filePath = filePath
)
