package ru.bot.awesomebot.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.bot.awesomebot.data.entity.UserToChat
import ru.bot.awesomebot.data.entity.UserToChatId

interface UserToChatRepository : JpaRepository<UserToChat, UserToChatId> {
    fun existsByChatIdAndUserId(chatId: Long, userId: Long): Boolean
    fun existsByChatIdAndUserIdAndBannedTrue(chatId: Long, userId: Long): Boolean
    fun findAllByChatIdAndActiveTrueAndPotdParticipantTrue(id: Long): List<UserToChat>
}
