package ru.bot.awesomebot.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.bot.awesomebot.data.entity.FunctionEntity
import ru.bot.awesomebot.data.entity.FunctionId

interface FunctionRepository : JpaRepository<FunctionEntity, FunctionId> {
    fun findByChatIdAndName(chatId: Long, name: String): FunctionEntity?
    fun findAllByChatId(chatId: Long): List<FunctionEntity>
    fun deleteByChatIdAndName(chatId: Long, name: String)
}
