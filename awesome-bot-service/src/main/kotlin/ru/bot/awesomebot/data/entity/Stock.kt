package ru.bot.awesomebot.data.entity

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "stock")
data class Stock(
    @Column(name = "chat_id", insertable = false, updatable = false)
    val chatId: Long,

    @Column(name = "key", insertable = false, updatable = false)
    val key: String,

    @EmbeddedId
    val id: StockId = StockId(chatId, key),

    @Column(name = "ticker")
    val ticker: String,

    @Column(name = "figi")
    val figi: String?,
)

@Embeddable
data class StockId(
    @Column(name = "chat_id") val chatId: Long,
    @Column(name = "key") val key: String
) : Serializable
