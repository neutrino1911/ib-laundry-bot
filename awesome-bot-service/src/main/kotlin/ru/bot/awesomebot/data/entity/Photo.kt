package ru.bot.awesomebot.data.entity

data class Photo(
        val fileId: String,
        val width: Int,
        val height: Int,
        val fileSize: Int,
        val filePath: String
)
