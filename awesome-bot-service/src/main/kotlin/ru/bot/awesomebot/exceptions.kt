package ru.bot.awesomebot

import ru.bot.awesomebot.core.BusinessException

class BanException(message: String) : BusinessException(message)

class CurrencyNotFoundException(
        private val currencies: List<String>
) : BusinessException("Currencies not found") {
    override val message: String
        get() {
            val builder = StringBuilder(32)
            if (currencies.size == 1) {
                builder.append("Currency not found: `").append(currencies[0]).append('`')
            } else {
                builder.append("Currencies not found: `")
                        .append(currencies[0])
                        .append(' ')
                        .append(currencies[1])
                        .append('`')
            }
            return builder.toString()
        }
}
