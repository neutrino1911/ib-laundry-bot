package ru.bot.awesomebot.component.handler

import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.CurrencyNotFoundException
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.service.CurrencyExchangeService
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

@Component
class CurrencyHandler(private val currencyExchangeService: CurrencyExchangeService) {

    private val formatter = DecimalFormat("#,##0.00").apply {
        val dfs = DecimalFormatSymbols()
        dfs.decimalSeparator = '.'
        dfs.groupingSeparator = ' '
        decimalFormatSymbols = dfs
    }

    @CommandMapping("/cur", parseMode = ParseMode.MARKDOWN)
    fun handle(message: Message): String {
        val split = message.text.split(" ")
        if (validateInput(split)) {
            try {
                if (split.size == 3) {
                    val source = split[1].toUpperCase()
                    val target = split[2].toUpperCase()
                    val rate = currencyExchangeService.getRate(source, target)
                    return String.format("" +
                            "%1\$s/%2\$s%n" +
                            "Rate: `%3\$s`%n" +
                            "Last update: `%4\$s`",
                            source,
                            target,
                            rate.toPlainString(),
                            currencyExchangeService.ratesTime
                    )
                } else {
                    val source = split[2].toUpperCase()
                    val target = split[3].toUpperCase()
                    val rate = currencyExchangeService.getRate(source, target)
                    val amount = BigDecimal(split[1])
                    val newAmount = amount.multiply(rate).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros()
                    return String.format("" +
                            "%1\$s/%2\$s%n" +
                            "Rate: `%3\$s`%n" +
                            "Amount: `%4\$s %1\$s`%n" +
                            "Result: `%5\$s %2\$s`%n" +
                            "Last update: `%6\$s`",
                            source,
                            target,
                            rate.toPlainString(),
                            formatter.format(amount),
                            formatter.format(newAmount),
                            currencyExchangeService.ratesTime
                    )
                }
            } catch (ex: CurrencyNotFoundException) {
                return ex.message
            }
        } else {
            return """
                Converts one currency to another.
                Syntax: `/cur [<money amount>] <from> <to>`
                Example: `/cur 100.50 EUR USD`
                """.trimIndent()
        }
    }

    private fun validateInput(input: List<String>): Boolean {
        return (input.size == 4 && input[1].matches("\\d+(\\.\\d+)?".toRegex()) && !StringUtils.isAnyBlank(input[2], input[3])
                || input.size == 3 && !StringUtils.isAnyBlank(input[1], input[2]))
    }

}
