package ru.bot.awesomebot.component.handler

import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.event.MessageEvent
import ru.bot.awesomebot.core.util.sendMessage
import ru.bot.awesomebot.service.AliasService
import ru.bot.awesomebot.service.BanService
import ru.bot.awesomebot.service.StockService

@Service
class KeywordService(
    private val bot: TelegramBot,
    private val banService: BanService,
    private val aliasService: AliasService,
    private val stockService: StockService,
) {
    @EventListener(MessageEvent::class)
    fun handle(event: MessageEvent) {
        val message = event.message
        if (banService.isBanned(message.chatId, message.from.id)) return
        message.text?.toLowerCase()?.let { text ->
            if (text.startsWith('/')) return
            val chat = message.chat
            (aliasService.getAliases(chat) + stockService.getAliases(chat))
                .map { it.toLowerCase() }
                .distinct()
                .forEach { alias ->
                    if (text.contains(alias)) {
                        (aliasService.get(chat, alias) ?: stockService.get(chat, alias))?.let {
                            bot.sendMessage(message) {
                                this.text = it
                            }
                        }
                    }
                }
        }
    }
}
