package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.aop.AdminOnly
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.getMention
import ru.bot.awesomebot.service.BanService

@Component
class BanHandler(private val banService: BanService) {

    @AdminOnly
    @CommandMapping("/ban", parseMode = ParseMode.MARKDOWN)
    fun ban(message: Message, args: CmdArgs): String? {
        val user = if (args.isEmpty()) {
            val userId = message.replyToMessage?.from?.id ?: return null
            banService.ban(message.chatId, userId)
        } else {
            val term = args[0]
            banService.ban(message.chatId, term)
        }
        return "${user.getMention()} забанен по причине пидорас!"
    }

    @AdminOnly
    @CommandMapping("/unban", parseMode = ParseMode.MARKDOWN)
    fun unban(message: Message, args: CmdArgs): String? {
        val user = if (args.isEmpty()) {
            val userId = message.replyToMessage?.from?.id ?: return null
            banService.unban(message.chatId, userId)
        } else {
            val term = args[0]
            banService.unban(message.chatId, term)
        }
        return "${user.getMention()} разбанен по причине натурал!"
    }

    @CommandMapping("/banlist", parseMode = ParseMode.MARKDOWN, notification = false)
    fun banlist(message: Message, args: CmdArgs): String {
        val users = banService.banList(message.chatId)
        return if (users.isEmpty()) "Список пуст!"
        else buildString {
            append("Забаненные пидоры:\n")
            for (user in users) append(user.getMention()).append('\n')
        }
    }

}
