package ru.bot.awesomebot.component.event.listener

import mu.KotlinLogging
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import ru.bot.awesomebot.core.event.NewChatMembersEvent
import ru.bot.awesomebot.data.repository.UserRepository
import ru.bot.awesomebot.service.UserToChatService

private val log = KotlinLogging.logger {}

@Component
class NewChatMembersEventListener(
        private val userRepository: UserRepository,
        private val userToChatService: UserToChatService
) {
    @EventListener(NewChatMembersEvent::class)
    fun onApplicationEvent(event: NewChatMembersEvent) {
        val message = event.update.message
        for (member in message.newChatMembers) {
            try {
                val chatId = message.chatId
                val userId = member.id
                if (userRepository.existsById(userId)) {
                    userToChatService.activate(chatId, userId)
                }
            } catch (e: Exception) {
                log.error(e.message, e)
            }
        }
    }
}
