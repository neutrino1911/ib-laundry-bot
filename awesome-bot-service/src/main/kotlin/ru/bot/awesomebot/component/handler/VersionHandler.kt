package ru.bot.awesomebot.component.handler

import org.springframework.boot.info.BuildProperties
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode

@Component
class VersionHandler(private val buildProperties: BuildProperties) {

    @CommandMapping("/version", parseMode = ParseMode.MARKDOWN)
    fun version(message: Message): String {
        return """
            build.artifact=`${buildProperties.artifact}`
            build.time=`${buildProperties.time}`
            build.version=`${buildProperties.version}`
        """.trimIndent()
    }

}
