package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.aop.OnlyOneAtTheMoment
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.getMention
import ru.bot.awesomebot.service.PotdService
import ru.bot.awesomebot.service.PotdStatisticService

@Component
class PotdHandler(
        private val potdService: PotdService,
        private val potdStatisticService: PotdStatisticService
) {

    @OnlyOneAtTheMoment
    @CommandMapping("/play", parseMode = ParseMode.MARKDOWN)
    fun play(message: Message): String {
        return potdService.getWinner(message.chatId)
                ?.let {
                    "Согласно моей информации, по результатам сегодняшнего розыгрыша, пидор дня - ${it.getMention()}!"
                }
                ?: potdService.play(message.chatId)?.let { "Пидор дня - ${it.getMention()}!" }
                ?: "Что-то пошло не так..."
    }

    @CommandMapping("/register")
    fun register(message: Message): String? {
        if (message.from.isBot || message.chat.isUserChat) {
            return null
        }
        return if (potdService.register(message.chatId, message.from.id)) {
            "Ты в игре!"
        } else {
            "Ты уже в игре!"
        }
    }

    @CommandMapping("/potdstats", parseMode = ParseMode.MARKDOWN, notification = false)
    fun potdstats(message: Message, args: CmdArgs): String {
        return if (message.from.id == 393711982L) {
            "Ты топ 1 пидор форева!"
        } else {
            val count = if (args.firstOrNull()?.matches("^\\d+$".toRegex()) == true) {
                args.first().toInt()
            } else 10
            potdStatisticService.fetchTop(message.chatId, count, false)
        }
    }

    @CommandMapping("/potdall", parseMode = ParseMode.MARKDOWN, notification = false)
    fun potdall(message: Message, args: CmdArgs): String {
        return if (message.from.id == 393711982L) {
            "Ты топ 1 пидор форева!"
        } else {
            val count = if (args.firstOrNull()?.matches("^\\d+$".toRegex()) == true) {
                args.first().toInt()
            } else 10
            potdStatisticService.fetchTop(message.chatId, count, true)
        }
    }

}
