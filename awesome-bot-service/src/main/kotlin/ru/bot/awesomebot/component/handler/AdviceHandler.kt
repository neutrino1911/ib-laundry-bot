package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.service.AdviceService

@Component
class AdviceHandler(private val adviceService: AdviceService) {

    @CommandMapping("/advice")
    fun handle(message: Message): String {
        val advice = adviceService.getAdvice(message.from.id)
        return wrapBlyat(advice.text)
    }

    private fun wrapBlyat(text: String): String {
        val builder = StringBuilder(text)
        if (builder.indexOf("Блять") == 0 && builder[5] != ',') {
            builder.insert(5, ',')
        } else if (builder.indexOf("блять") != -1) {
            val idx = builder.indexOf("блять")
            if (builder[idx + 5] != ',' && idx + 6 < text.length) {
                builder.insert(idx + 5, ',')
            }
            if (builder[idx - 2] != ',') {
                builder.insert(idx - 1, ',')
            }
        }
        return builder.toString()
    }

}
