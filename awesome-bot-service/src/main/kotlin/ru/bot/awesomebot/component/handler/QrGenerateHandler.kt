//package ru.bot.awesomebot.component.handler
//
//import com.google.zxing.BarcodeFormat
//import com.google.zxing.EncodeHintType
//import com.google.zxing.client.j2se.MatrixToImageWriter
//import com.google.zxing.qrcode.QRCodeWriter
//import mu.KotlinLogging
//import org.apache.commons.lang3.StringUtils
//import org.springframework.stereotype.Component
//import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
//import org.telegram.telegrambots.meta.api.objects.Message
//import ru.bot.awesomebot.core.TelegramBot
//import ru.bot.awesomebot.core.dispatching.CommandHandler
//import java.io.ByteArrayInputStream
//import java.io.ByteArrayOutputStream
//
//private val log = KotlinLogging.logger {}
//
//@Component
//class QrGenerateHandler(
//        private val bot: TelegramBot
//) : CommandHandler {
//
//    override fun getCommand() = "/qrgenerate"
//
//    override fun handle(message: Message) {
//        val text = getText(message.text)
//        if (StringUtils.isBlank(text)) {
//            return
//        }
//        val bytes = generate(text) ?: return
//        val sendPhoto = SendPhoto()
//        sendPhoto.setChatId(message.chatId)
//        sendPhoto.replyToMessageId = message.messageId
//        sendPhoto.setPhoto("qrcode.png", ByteArrayInputStream(bytes))
//        bot.execute(sendPhoto)
//    }
//
//    private fun getText(text: String): String? {
//        return if (text.split(' ').size < 2) null else text.substringAfter(' ')
//    }
//
//    private fun generate(text: String?): ByteArray? {
//        try {
//            val qrCodeWriter = QRCodeWriter()
//            val hints = mapOf(EncodeHintType.CHARACTER_SET to "UTF-8")
//            val bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, 200, 200, hints)
//            ByteArrayOutputStream().use { out ->
//                MatrixToImageWriter.writeToStream(bitMatrix, "PNG", out)
//                return out.toByteArray()
//            }
//        } catch (e: Exception) {
//            log.error(e.message, e)
//            return null
//        }
//    }
//
//}
