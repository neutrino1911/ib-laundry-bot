package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.aop.AdminOnly
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.data.repository.UserRepository
import ru.bot.awesomebot.getMention

@Component
class AllHandler(private val userRepository: UserRepository) {

    @AdminOnly
    @CommandMapping("/all", parseMode = ParseMode.MARKDOWN)
    fun handle(message: Message): String {
        val users = userRepository.findAllByChatId(message.chatId)
        return buildString {
            if (message.text.contains(' ')) append(message.text.substringAfter(' ')).append('\n')
            users.forEach { append(it.getMention()).append(' ') }
        }
    }

}
