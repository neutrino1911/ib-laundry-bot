package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.core.util.sendPhoto
import ru.bot.awesomebot.service.FunpicsService

@Component
class FunpicsCommandHandler(
        private val bot: TelegramBot,
        private val funpicsService: FunpicsService
) {

    @CommandMapping("/funpics", parseMode = ParseMode.MARKDOWN, notification = false)
    fun funpics(message: Message) {
        val str = message.text
        if (str.contains(' ')) {
            val text = str.substringAfter(' ').replace("[^а-яёА-ЯЁa-zA-Z0-9 .,!?:;()-]".toRegex(), "")
            val filename = "$text.jpg"
            val bytes = funpicsService.generateFunpics(text)
            bot.sendPhoto(message, filename, bytes)
        }
    }

    @CommandMapping("/memasiki", parseMode = ParseMode.MARKDOWN)
    fun memasiki(message: Message, args: CmdArgs) {
        if (args.isNotEmpty()) {
            val templateNumber = getTemplateNumber(args)
            val text = getText(args).replace("[^а-яёА-ЯЁa-zA-Z0-9_]".toRegex(), "")
            val bytes = funpicsService.generateMemasiki(templateNumber, text)
            val filename = "$text.jpg"
            bot.sendPhoto(message, filename, bytes)
        }
    }

    private fun getText(commandWords: List<String>): String =
            if (commandWords.size == 1) commandWords[0] else commandWords[1]

    private fun getTemplateNumber(commandWords: List<String>): String =
            if (commandWords.size > 1) commandWords[0] else "1"

}
