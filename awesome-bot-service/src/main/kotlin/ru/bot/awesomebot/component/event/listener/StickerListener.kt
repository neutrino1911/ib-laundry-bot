package ru.bot.awesomebot.component.event.listener

import mu.KotlinLogging
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.config.TelegramBotProperties
import ru.bot.awesomebot.core.event.MessageEvent
import ru.bot.awesomebot.core.util.deleteMessage
import ru.bot.awesomebot.core.util.getFileHash
import ru.bot.awesomebot.core.util.isFromAdmin
import ru.bot.awesomebot.service.StickerBanService

private val log = KotlinLogging.logger {}

@Component
class StickerListener(
        private val bot: TelegramBot,
        private val props: TelegramBotProperties,
        private val stickerBanService: StickerBanService
) {
    @Async
    @EventListener(MessageEvent::class)
    fun onApplicationEvent(event: MessageEvent) {
        val message = event.message
        if (message.isUserMessage || bot.isFromAdmin(message)) {
            return
        }
        val chat = message.chat
        if (!chat.isUserChat && message.hasSticker() && isBanned(message)) {
            try {
                bot.deleteMessage(message)
            } catch (e: TelegramApiRequestException) {
                log.error(e.toString(), e)
            } catch (e: Exception) {
                log.error(e.message, e)
            }
        }
    }

    private fun isBanned(message: Message): Boolean {
        val chatId = message.chat.id
        val sticker = message.sticker
        return (stickerBanService.isBanned(chatId, sticker.setName)
                || stickerBanService.isBanned(chatId, bot.getFileHash(sticker.fileId, props.token)))
    }

}
