package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.BanException
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.aop.AdminOnly
import ru.bot.awesomebot.core.config.TelegramBotProperties
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.util.deleteMessage
import ru.bot.awesomebot.core.util.getFileHash
import ru.bot.awesomebot.service.StickerBanService

@AdminOnly
@Component
@Transactional
class StickerBanHandler(
        private val bot: TelegramBot,
        private val service: StickerBanService,
        private val props: TelegramBotProperties
) {

    @CommandMapping("/bansticker")
    fun bansticker(message: Message): Any? {
        val replyToMessage = message.replyToMessage
        if (!replyToMessage.hasSticker()) {
            return null
        }
        return try {
            val fileId = replyToMessage.sticker.fileId
            val hash = bot.getFileHash(fileId, props.token)
            service.ban(message.chat.id, hash)
            deleteMessage(message) {
                messageId = replyToMessage.messageId
            }
        } catch (e: BanException) {
            e.message
        } catch (e: Exception) {
            "Не шмогла!"
        }
    }

    @CommandMapping("/unbansticker")
    fun unbansticker(message: Message): String? {
        val replyToMessage = message.replyToMessage
        if (!replyToMessage.hasSticker()) {
            return null
        }
        return try {
            val fileId = replyToMessage.sticker.fileId
            val hash = bot.getFileHash(fileId, props.token)
            service.unban(message.chat.id, hash)
            "Стикер разбанен"
        } catch (e: BanException) {
            e.message
        } catch (e: Exception) {
            "Не шмогла!"
        }
    }

    @CommandMapping("/banpack")
    fun banpack(message: Message): Any? {
        val replyToMessage = message.replyToMessage
        if (!replyToMessage.hasSticker()) {
            return null
        }
        return try {
            val packName = replyToMessage.sticker.setName
            service.banPack(message.chat.id, packName)
            deleteMessage(message) {
                messageId = replyToMessage.messageId
            }
        } catch (e: BanException) {
            e.message
        } catch (e: Exception) {
            "Не шмогла!"
        }
    }

    @CommandMapping("/unbanpack")
    fun unbanpack(message: Message): String? {
        val replyToMessage = message.replyToMessage
        if (!replyToMessage.hasSticker()) {
            return null
        }
        return try {
            val packName = replyToMessage.sticker.setName
            service.unbanPack(message.chat.id, packName)
            "Пак разбанен"
        } catch (e: BanException) {
            e.message
        } catch (e: Exception) {
            "Не шмогла!"
        }
    }

}
