package ru.bot.awesomebot.component.handler

import org.mariuszgromada.math.mxparser.Expression
import org.mariuszgromada.math.mxparser.PrimitiveElement
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.data.entity.FunctionEntity
import ru.bot.awesomebot.data.repository.FunctionRepository
import java.math.BigDecimal
import java.math.RoundingMode

typealias MpFunction = org.mariuszgromada.math.mxparser.Function

@Service
class CalcHandler(private val functionRepository: FunctionRepository) {

    @CommandMapping("/calc", parseMode = ParseMode.MARKDOWN)
    fun calc(message: Message, args: CmdArgs): String? {
        val text = message.text.trim()
        return when {
            args.isEmpty() -> help()
            "set" == args[0] -> {
                val function = extractFunction(text, message.chatId)
                if (function == null) {
                    "Синтаксис проверь!"
                } else {
                    val name = function.substringBefore('(')
                    functionRepository.findByChatIdAndName(message.chatId, name)
                            ?.let { functionRepository.save(it.copy(value = function)) }
                            ?: run {
                                val entity = FunctionEntity(chatId = message.chatId, name = name, value = function)
                                functionRepository.save(entity)
                            }
                    "Схоронил..."
                }
            }
            "del" == args[0] -> {
                functionRepository.deleteByChatIdAndName(message.chatId, args[1])
                "Done"
            }
            "list" == args[0] -> {
                val functions = functionRepository.findAllByChatId(message.chatId).sortedBy { it.name }
                functions.joinToString("`\n`", "`", "`") { it.value }
            }
            else -> calc(text, findFunctions(message.chatId))
        }
    }

    private fun extractFunction(text: String, chatId: Long): String? {
        val func = text.substring(text.indexOf("set") + 4)
        val name = func.substringBefore('(')
        val functions = functionRepository.findAllByChatId(chatId)
                .filter { it.name != name }
                .map { MpFunction(it.value) }
        initFunctions(functions)
        val function = MpFunction(func, *functions.toTypedArray<PrimitiveElement>())
        return if (function.checkSyntax()) func else null
    }

    private fun calc(text: String, functions: List<org.mariuszgromada.math.mxparser.Function>): String? {
        val exp = text.substringAfter(' ')
        val expression = Expression(exp, *functions.toTypedArray<PrimitiveElement>())
        val result = expression.calculate()
        return if (result.isNaN()) null
        else BigDecimal(result).setScale(4, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString()
    }

    private fun findFunctions(chatId: Long): List<MpFunction> {
        val functions = functionRepository.findAllByChatId(chatId).map { MpFunction(it.value) }
        initFunctions(functions)
        return functions
    }

    private fun initFunctions(functions: List<MpFunction>) {
        for (func in functions) {
            if (!func.checkSyntax()) {
                func.addFunctions(*(functions - func).toTypedArray())
            }
        }
    }

    private fun help(): String {
        return """
            /calc <expression> - calculate given expression
            eg: `/calc 12 / (3 * 2)`
            /calc set <function> - save given function for later use
            eg: `/calc set fun(x) = x * 42` then `/calc fun(7)`
            /calc del <function name> - delete function
            eg: /calc del fun
            /calc list - show all saved functions
            """.trimIndent()
    }

}
