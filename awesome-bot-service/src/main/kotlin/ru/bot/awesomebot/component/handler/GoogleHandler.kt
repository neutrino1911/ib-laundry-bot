package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@Component
class GoogleHandler {

    @CommandMapping("/google", parseMode = ParseMode.MARKDOWN)
    fun handle(message: Message): String? {
        return getQuery(message.text)?.let { getText(it) }
    }

    private fun getText(query: String): String {
        return String.format("[Google](%2\$s%1\$s)%n[DuckDuckGo](%3\$s%1\$s)%n[Yandex](%4\$s%1\$s)",
                query,
                GOOGLE_ROOT,
                DUCK_ROOT,
                YANDEX_ROOT
        )
    }

    private fun getQuery(str: String): String? {
        val idx = str.indexOf(' ') + 1
        return if (idx != 0) {
            URLEncoder.encode(str.substring(idx), StandardCharsets.UTF_8)
        } else null
    }

    companion object {
        private const val GOOGLE_ROOT = "https://www.google.com/search?ie=UTF-8&q="
        private const val DUCK_ROOT = "https://duckduckgo.com/?q="
        private const val YANDEX_ROOT = "https://www.yandex.ru/search/?text="
    }

}
