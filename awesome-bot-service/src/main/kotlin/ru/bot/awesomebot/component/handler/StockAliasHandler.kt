package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.service.StockService

@Component
class StockAliasHandler(
    private val stockService: StockService
) {
    @CommandMapping("/stocks", parseMode = ParseMode.HTML, notification = false)
    fun aliases(message: Message): String {
        val aliases = stockService.getAliases(message.chat).joinToString(separator = ", ")
        return "Available gets:\n\n$aliases"
    }

    @CommandMapping("/stock/del")
    fun del(message: Message, args: CmdArgs): String {
        val key = args[0]
        stockService.del(message.chat, key)
        return "OK"
    }

    @CommandMapping("/stock")
    fun get(message: Message, args: CmdArgs): String {
        val key = args[0]
        return stockService.get(message.chat, key) ?: "Key '$key' not found"
    }

    @CommandMapping("/stock/set")
    fun set(message: Message, args: CmdArgs): String {
        val key = args[0]
        val value = message.text.substringAfter("$key ").trim()
        stockService.set(message.chat, key, value)
        return "OK"
    }
}
