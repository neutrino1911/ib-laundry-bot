package ru.bot.awesomebot.component.handler

import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.event.MessageEvent
import ru.bot.awesomebot.core.util.sendMessage
import java.util.concurrent.ConcurrentHashMap
import kotlin.random.Random

@Component
class FenceHandler(private val bot: TelegramBot) {
    private val settings = ConcurrentHashMap<Long, Int>()

    @CommandMapping("/fence")
    fun fence(message: Message): String? {
        val text = if (message.replyToMessage != null)
            message.replyToMessage.text
        else
            message.text.substringAfter("/fence ")

        return getFence(text)
    }

    @CommandMapping("/fence/random")
    fun fenceRandom(message: Message, args: CmdArgs): String {
        if (args.isEmpty()) {
            return (settings[message.chatId] ?: 10).toString()
        } else if (args[0].toIntOrNull() != null) {
            val rnd = args[0].toInt()
            if (rnd < 0 || rnd > 100) return "Значение должно быть в диапазоне 0..100"
            settings[message.chatId] = rnd
            return "Новое значение установлено"
        } else {
            return "[Ошибка]"
        }
    }

    @EventListener(MessageEvent::class)
    fun messageHandler(event: MessageEvent) {
        val rnd = settings[event.message.chatId] ?: 10
        if (rnd == 0) return
        if (Random.nextInt(0, 100) <= rnd) {
            getFence(event.message.text)?.let {
                bot.sendMessage(event.message) {
                    this.text = it
                }
            }
        }
    }

    private fun getFence(text: String): String? {
        val charArray = text.toCharArray()
        var counter = 0
        charArray.forEachIndexed { idx, ch ->
            if (ch.toLowerCase() != ch.toUpperCase()) {
                if (counter++ % 2 == 0) charArray[idx] = ch.toLowerCase()
                else charArray[idx] = ch.toUpperCase()
            }
        }
        if (counter < 3) return null
        return String(charArray)
    }
}
