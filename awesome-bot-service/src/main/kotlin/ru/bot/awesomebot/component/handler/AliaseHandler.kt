package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode
import ru.bot.awesomebot.service.AliasService

@Component
class AliaseHandler(
    private val aliasService: AliasService,
) {
    @CommandMapping("/aliases", parseMode = ParseMode.HTML, notification = false)
    fun aliases(message: Message): String {
        val aliases = aliasService.getAliases(message.chat).joinToString(separator = ", ")
        return "Available gets:\n\n$aliases"
    }

    @CommandMapping("/del")
    fun del(message: Message, args: CmdArgs): String {
        val key = args[0]
        aliasService.del(message.chat, key)
        return "OK"
    }

    @CommandMapping("/get")
    fun get(message: Message, args: CmdArgs): String {
        val key = args[0]
        val value = aliasService.get(message.chat, key)
        return if (value == null) "Key '$key' not found" else "$key\n$value"
    }

    @CommandMapping("/set")
    fun set(message: Message, args: CmdArgs): String {
        val key = args[0]
        val value = message.text.substringAfter("$key ").trim()
        aliasService.set(message.chat, key, value)
        return "OK"
    }
}
