package ru.bot.awesomebot.aop

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.aop.UserChatRefExtractor
import ru.bot.awesomebot.core.util.isAdmin
import ru.bot.awesomebot.service.BanService

@Aspect
@Component
@Order(-20)
class BanAdvice(
        private val bot: TelegramBot,
        private val banService: BanService,
        private val extractor: UserChatRefExtractor
) {
    @Around("@annotation(ru.bot.awesomebot.core.dispatching.CommandMapping)")
    fun around(pjp: ProceedingJoinPoint): Any? {
        val (chatId, userId) = extractor.extract(pjp) ?: return pjp.proceed()

        return if (banService.isBanned(chatId, userId) && !bot.isAdmin(chatId, userId)) {
            return null
        } else {
            pjp.proceed()
        }
    }
}
